﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author: Michael Nelson
//Class Projectile is for givign the projectile damage and to actually hurt targets
public class Projectile : MonoBehaviour {

    //damage the bullet does
    public int damage;

    private void OnCollisionEnter(Collision collision)
    {
        //If you hit the enemy
        if (collision.gameObject.CompareTag("Enemy"))
        {
            //Take damage from wherever the enemy health is stored
            collision.gameObject.GetComponent<MissingComponentException>();
        }

        //Destroy the object no matter what it hits
        Destroy(this.gameObject);
    }
}
