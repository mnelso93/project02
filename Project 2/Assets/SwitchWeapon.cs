﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Author: Michael Nelson
//class SwitchWeapon manages the ability to switch between weapons
public class SwitchWeapon : MonoBehaviour {

    public GameObject[] guns;

	// Use this for initialization
	void Start ()
    {
        //Find all the guns
        guns = GameObject.FindGameObjectsWithTag("gun");
        for (int i = 0; i < guns.Length; i++)
        {
            //set the basic weapon as active, disable the rest
            Debug.Log(guns[i]);
            if (i != 2)
            {
                guns[i].GetComponent<GunsWithAmmo>().isActive = false;
                guns[i].SetActive(false);
            }

            else
            {
                guns[i].GetComponent<Snowball>().isActive = true;
                guns[i].SetActive(true);
            }
        }

	}
	
	// Update is called once per frame
	void Update ()
    {
        //Check if weapon 1 is selected
		if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            WeaponSelector(2);
        }

        //Check if weapon 2 is selected
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            WeaponSelector(3);
        }

        //Check if weapon 3 is selected
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            WeaponSelector(0);
        }

        //Check if weapon 4 is selected
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            WeaponSelector(1);
        }

        for (int i = 0; i < guns.Length; i++)
        {
            if (i != 2)
            {
                if (guns[i].GetComponent<GunsWithAmmo>().levelAvailable > SceneManager.GetActiveScene().buildIndex - 1)
                {
                    guns[i].SetActive(false);
                }
            }
        }
    }

    /// <summary>
    /// This cycles through and compares each index to the input given to cycle through the weapons
    /// </summary>
    /// <param name="input">The given number to compare each gun to</param>
    private void WeaponSelector(int input)
    {
        //Compare the weapon ID to input provided
        for(int i = 0; i < guns.Length; i++)
        {
            //Set it as the active weapon
            if(input == i)
            {
                //Advanced weapon
                if (i != 2)
                {
                    guns[i].SetActive(true);
                    guns[i].GetComponent<GunsWithAmmo>().isActive = true;
                }

                //Starter weapon
                else
                {
                    guns[i].SetActive(true);
                    guns[i].GetComponent<Snowball>().isActive = true;
                }
            }

            //Disable it completely
            else
            {
                //Advanced weapon
                if (i != 2)
                {
                    guns[i].GetComponent<GunsWithAmmo>().isActive = false;
                }

                //Starter weapon
                else
                {
                    guns[i].GetComponent<Snowball>().isActive = false;
                }
                guns[i].SetActive(false);
            }
        }
    }
}
